package com.rsps;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.jsoup.Connection;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import jdk.internal.org.objectweb.asm.tree.analysis.Value;

public class ItemDataDump {

	private static List<Definition> itemDefinitions = new ArrayList<Definition>();

	public static void main(String[] args) throws JsonSyntaxException, JsonIOException, FileNotFoundException {
		ItemType[] items = new GsonBuilder().create().fromJson(new FileReader("./data/client_item_data.json"),
				ItemType[].class);

		long start = System.currentTimeMillis();
		Stream.of(items).parallel().filter(Objects::nonNull).forEach(itemType -> {
			String name = itemType.name;
			if (name == null || name.isEmpty() || name.equals("null")) {
				return;
			}
			name = name.replaceAll(" ", "_");
			String url = "http://oldschoolrunescape.wikia.com/wiki/" + name;
			System.out.println("Opening: " + url);
			try {
				Connection connection = Jsoup.connect(url);
				if (connection.response().statusCode() == 404) {
					return;
				}
				Document doc = connection.get();

				Element infoBox = doc.select(".infobox-wrapper").first();
				if (infoBox == null) {
					System.out.println("Data not found for url: " + url);
					return;
				}

				Elements linkElements = doc.select("a");
				Elements imageElements = doc.select("img");
				Definition definition = parseData(itemType, linkElements, imageElements);
				if (definition != null) {
					itemDefinitions.add(definition);
				}
			} catch (Exception e) {
				if (!(e instanceof HttpStatusException))
					e.printStackTrace();
			}
		});

		itemDefinitions.forEach(definition -> {
			if (Arrays.stream(definition.bonuses).filter(i -> i != 0).count() <= 0) {
				definition.bonuses = null;
			}
		});

		itemDefinitions.sort((d1, d2) -> Integer.compare(d1.id, d2.id));

		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		try (FileWriter fw = new FileWriter("./data/item_data.json")) {
			gson.toJson(itemDefinitions, fw);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		long elapsed = System.currentTimeMillis() - start;
		System.out.println("Time Elapsed: " + (elapsed / 1000) + " seconds.");
	}

	private static Definition parseData(ItemType itemType, Elements linkElements, Elements imageElements) {
		int itemID = itemType.id;
		String name = itemType.name;
		int highAlchemy = 0;
		int lowAlchemy = 0;
		boolean tradeable = true;
		boolean destroyable = false;
		int storePrice = 0;
		float weight = 0;
		String examine = "";
		String itemSlot = "";
		int[] bonuses = new int[15];

		try {
			Element highAlchemyElement = linkElements.select("[title='High Level Alchemy']").first().parent().parent()
					.selectFirst("td");
			if (highAlchemyElement != null) {
				String value = extractNumbers(highAlchemyElement.text());
				if (value != null && !value.isEmpty()) {
					highAlchemy = Integer.parseInt(value);
				}
			}
		} catch (Exception ex) {
			System.err.println("Error parsing data [" + itemID + "]: " + name);
			ex.printStackTrace();
		}

		try {
			Element lowAlchParent = linkElements.select("[title='Low Level Alchemy']").first();
			if (lowAlchParent != null) {
				Element lowAlchemyElement = lowAlchParent.parent().parent().selectFirst("td");
				if (lowAlchemyElement != null) {
					String value = extractNumbers(lowAlchemyElement.text());
					if (value != null && !value.isEmpty()) {
						lowAlchemy = Integer.parseInt(value);
					}
				}
			}
		} catch (Exception ex) {
			System.err.println("Error parsing data [" + itemID + "]: " + name);
			ex.printStackTrace();
		}

		try {
			Element tradeableElement = linkElements.select("[title='Tradeable']").first().parent().parent()
					.selectFirst("td");
			if (tradeableElement != null) {
				String value = tradeableElement.text();
				if (value != null && !value.isEmpty()) {
					tradeable = !value.equals("No");
				}
			}
		} catch (Exception ex) {
			System.err.println("Error parsing data [" + itemID + "]: " + name);
			ex.printStackTrace();
		}

		try {
			Element destroyTypeElement = linkElements.select("[title='Destroy']").first().parent().parent()
					.selectFirst("td");
			if (destroyTypeElement != null) {
				String value = destroyTypeElement.text();
				if (value != null && !value.isEmpty()) {
					destroyable = !value.equals("Drop");
				}
			}
		} catch (Exception ex) {
			System.err.println("Error parsing data [" + itemID + "]: " + name);
			ex.printStackTrace();
		}

		try {
			Element storePriceElement = linkElements.select("[title='Prices']").select("a:contains(Store price)")
					.first().parent().parent().selectFirst("td");
			if (storePriceElement != null) {
				String text = storePriceElement.text();
				if (text != null && !text.isEmpty()) {
					String priceValue = extractNumbers(text);
					if (priceValue.isEmpty()) {
						storePrice = 0;
					} else {
						storePrice = Integer.parseInt(priceValue);
					}
				}
			}
		} catch (Exception ex) {
			System.err.println("Error parsing data [" + itemID + "]: " + name);
			ex.printStackTrace();
		}

		try {
			Element weightElement = linkElements.select("[title='Weight']").first().parent().parent().selectFirst("td");
			if (weightElement != null) {
				String value = extractNumbers(weightElement.text());
				if (value != null && !value.isEmpty()) {
					weight = Float.parseFloat(value);
				}
			}
		} catch (Exception ex) {
			System.err.println("Error parsing data [" + itemID + "]: " + name);
			ex.printStackTrace();
		}

		try {
			Element examineElement = linkElements.select("[title='Examine']").first().parent().parent()
					.nextElementSibling().select("td").first();
			if (examineElement != null) {
				String value = examineElement.text();
				if (value != null && !value.isEmpty()) {
					examine = value;
				}
			}
		} catch (Exception ex) {
			System.err.println("Error parsing data [" + itemID + "]: " + name);
			ex.printStackTrace();
		}

		try {
			Element itemSlotElement = imageElements.select("[alt$='slot']").first();
			if (itemSlotElement != null) {
				String value = itemSlotElement.attr("alt");
				if (value != null && !value.isEmpty()) {
					itemSlot = value.replaceAll(" slot", "").toUpperCase();
				}
			}
		} catch (Exception ex) {
			System.err.println("Error parsing data [" + itemID + "]: " + name);
			ex.printStackTrace();
		}

		try {
			Element equipStatsElement = linkElements.select("a:contains(Bonuses)").first();
			if (equipStatsElement != null) {
				Elements bonusElements = equipStatsElement.parent().parent().select("tr > td");
				if (bonusElements != null) {
					List<Element> elementsList = bonusElements.stream()
							.filter(element -> element.children().size() == 0 && element.html().length() < 15)
							.collect(Collectors.toList());

					for (int i = 0; i < Math.min(15, elementsList.size()); i++) {
						Element bonusElement = elementsList.get(i);
						if (bonusElement == null)
							continue;
						String value = bonusElement.text();
						if (value != null && !value.isEmpty()) {
							try {
								bonuses[i] = Integer.parseInt(extractNumbers(value));
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}
				}
			}
		} catch (Exception ex) {
			System.err.println("Error parsing data [" + itemID + "]: " + name);
			ex.printStackTrace();
		}

		return new Definition(itemID, name, highAlchemy, lowAlchemy, tradeable, destroyable, storePrice, weight,
				examine, itemSlot, bonuses);
	}

	private static String extractNumbers(String value) {
		return value.replaceAll("[^\\d.-]", "");
	}
}

class Definition {
	int id;
	String name;
	int highAlch;
	int lowAlch;
	boolean tradeable;
	boolean destroyable;
	int storePrice;
	float weight;
	String examine;
	String slot;
	int bonuses[];

	Definition(int id, String name, int highAlch, int lowAlch, boolean tradeable, boolean destroyable, int storePrice,
			float weight, String examine, String slot, int[] bonuses) {
		this.id = id;
		this.name = name;
		this.highAlch = highAlch;
		this.lowAlch = lowAlch;
		this.tradeable = tradeable;
		this.destroyable = destroyable;
		this.storePrice = storePrice;
		this.weight = weight;
		this.examine = examine;
		this.slot = slot;
		this.bonuses = bonuses;
	}

	@Override
	public String toString() {
		return "Definition [id=" + id + ", name=" + name + ", highAlch=" + highAlch + ", lowAlch=" + lowAlch
				+ ", tradeable=" + tradeable + ", destroyable=" + destroyable + ", storePrice=" + storePrice
				+ ", weight=" + weight + ", examine=" + examine + ", slot=" + slot + ", bonuses="
				+ Arrays.toString(bonuses) + "]";
	}

}

class ItemType {
	int id;
	String name;
}